# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.7](https://bitbucket.org/honestdev/eslint-config/compare/v0.0.6...v0.0.7) (2020-03-27)

### [0.0.6](https://bitbucket.org/honestdev/eslint-config/compare/v0.0.5...v0.0.6) (2020-03-27)

### [0.0.5](https://bitbucket.org/honestdev/eslint-config/compare/v0.0.4...v0.0.5) (2020-03-27)

### [0.0.4](https://bitbucket.org/honestdev/eslint-config/compare/v0.0.3...v0.0.4) (2020-03-16)

### [0.0.3](https://bitbucket.org/honestdev/eslint-config/compare/v0.0.2...v0.0.3) (2020-03-03)

### [0.0.2](https://bitbucket.org/honestdev/eslint-config/compare/v0.0.1...v0.0.2) (2020-03-03)

### 0.0.1 (2020-03-03)
